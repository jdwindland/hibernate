package com.jdwindland;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//Main class that runs program.
public class Main {

//    int option;

    public static void main(String[] args) throws IOException {

	    BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
	    boolean valid = false;
	    boolean pass = false;

	    /*Asks user to select option. The option selected will determine if user
        sees list of pets in database table, adds a pet to the table, deletes
        a pet from the table or updates the information of a pet in the table.*/
        while(!pass) {
            try {
                System.out.println("\nEnter an option: \n"
                        + "1) List Pets \n"
                        + "2) Add a Pet \n"
                        + "3) Delete a Pet \n"
                        + "4) Update a Pet \n");

                int option = Integer.parseInt(reader.readLine());

                if (option == 1) {
                    valid = true;
                    pass = true;
                }
                if (option == 2) {
                    valid = true;
                    pass = true;
                }
                if (option == 3) {
                    valid = true;
                    pass = true;
                }
                if (option == 4) {
                    valid = true;
                    pass = true;
                }

                if (valid) {
                    switch (option) {
                        //Calls the method to see a list of the pets in table.
                        case 1 -> PetsDAO.listPets();
                        //Calls the method to add a pet to the table and then prints a new list.
                        case 2 -> {
                            PetsDAO.addPet("Dory", "Fish", 1, 'F');
                            PetsDAO.listPets();
                        }
                        //Calls the method to delete a pet from the table and then prints a new list.
                        case 3 -> {
                            PetsDAO.deletePet(15);
                            PetsDAO.listPets();
                        }
                        //Calls the method to update a pet's information and then prints a new list.
                        case 4 -> {
                            PetsDAO.updatePet(13, "Hammy", "Hamster", 3, 'F');
                            PetsDAO.listPets();
                        }
                    }
                } else {
                    System.out.println("\nPlease try again and enter a valid option.\n");
                }
            } catch (NumberFormatException e) {
                System.out.println("\nPlease try again and enter a valid option.\n");
            }
        }
    }
}
