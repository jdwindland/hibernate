package com.jdwindland;

//Class to create object that maps to database table.

import javax.persistence.*;

//Tells application to use hibernate.
@Entity
//Maps table to Java class.
@Table(name = "tbl_pets")
public class Pets {

    //Auto generates ID by assigning next value in the hibernate_sequence table.
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    //Maps each attribute to the column in tbl_pets table in database.
    @Column(name = "id")
    private int id;
    @Column (name = "name")
    private String name;
    @Column (name = "breed")
    private String breed;
    @Column (name = "age")
    private int age;
    @Column (name = "gender")
    private char gender;

    //Constructor methods.
    public Pets(){}

    public Pets(String name, String breed, int age, char gender){
        this.name = name;
        this.breed = breed;
        this.age = age;
        this.gender = gender;
    }

    //Setters and Getters for each attribute. Will be used by the PetsDAO class.
    public int getId(){
        return id;
    }

    public void setId(int id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    //Method to print the object. Used for initial testing.
    public String toString() {
        return id + " " + name + " " + breed + " " + age + " " + gender;
    }
}
