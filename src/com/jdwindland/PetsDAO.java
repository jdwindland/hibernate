package com.jdwindland;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class PetsDAO {

    //Creates a session factory.
    private static SessionFactory ourSessionFactory = null;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            ourSessionFactory = configuration.buildSessionFactory();
        } catch (HibernateException e) {
            System.out.println("\nUnable to create a session factory.\n");
            e.printStackTrace();
        }
    }

    //Method to access database and return list of pets.
    public static void listPets(){
        Transaction t = null;
        try (Session session = ourSessionFactory.openSession()) {
            t = session.beginTransaction();
            List pets = session.createQuery("From Pets").list();
            System.out.println("\n------------------------------------------");
            System.out.println("ID\t\tName\t\tBreed\tAge\t\tGender");
            System.out.println("------------------------------------------");
            for (Object o : pets) {
                Pets pet = (Pets) o;
                System.out.print(pet.getId() + "\t\t" + pet.getName() +
                        "\t\t" + pet.getBreed() + "\t\t" + pet.getAge() +
                        "\t\t" + pet.getGender() + "\n");
            }
            t.commit();
        } catch (HibernateException e) {
            if (t != null) t.rollback();
            System.out.print("\nUnable to list pets.\n");
            e.printStackTrace();
        }
    }

    //Method to add a pet to the database.
    public static void addPet(String name, String breed, int age, char gender){
        Transaction t = null;
        Integer id = null;
        try (Session session = ourSessionFactory.openSession()) {
            t = session.beginTransaction();
            Pets pet = new Pets(name, breed, age, gender);
            id = (Integer) session.save(pet);
            t.commit();
        } catch (HibernateException e) {
            if (t != null) t.rollback();
            System.out.println("\nUnable to add the pet.\n");
            e.printStackTrace();
        }
    }

    //Method to delete pet from database.
    public static void deletePet(Integer id){
        Transaction t;
        try (Session session = ourSessionFactory.openSession()) {
            t = session.beginTransaction();
            Pets pet = session.get(Pets.class, id);
            session.delete(pet);
            t.commit();
        } catch (HibernateException e) {
            System.out.println("\nUnable to delete the pet.\n");
            e.printStackTrace();
        }
    }

    //method to update a specific pet in the database.
    public static void updatePet(Integer id, String name, String breed, int age, char gender){
        Transaction t;
        try (Session session = ourSessionFactory.openSession()) {
            t = session.beginTransaction();
            Pets pet = session.get(Pets.class, id);
            pet.setName(name);
            pet.setBreed(breed);
            pet.setAge(age);
            pet.setGender(gender);
            session.update(pet);
            t.commit();
        } catch (HibernateException e) {
            System.out.println("\nUnable to update the pet.\n");
            e.printStackTrace();
        }
    }
}
